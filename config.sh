#!/bin/sh

SITE="cdr"
REPO="git@bitbucket.org:2devinc/centre-du-rasoir.git"
SOURCE_FOLDER="sources"
DB_FOLDER="databases"
VAGRANT="vagrant"
WWW_FOLDER="/var/www"

## Shell
ohmyzsh=".oh-my-zsh"

## DB updtes
url="http://$SITE.test/"
url_ssl="https://$SITE.test/"
domain="$SITE.test"
email="david@2dev.ca"

## n98-magerun
FILE="/usr/local/bin/n98-magerun"
USERNAME="admin" 
EMAIL_ADMIN="admin@2dev.ca" 
PASSWORD_ADMIN="admin123" 
FIRSTNAME="Admin" 
LASTNAME="Admin"
ROLE="Administrators"
