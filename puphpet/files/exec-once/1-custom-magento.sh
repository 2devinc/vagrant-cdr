#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/../../../config.sh"

########## EXEC SQL QUERY ##########

query="UPDATE core_config_data SET value='${url_ssl}' WHERE path LIKE '%base_url%';
    UPDATE core_config_data SET value='${email}' WHERE path LIKE 'sales_email/order/copy_to';
    UPDATE core_config_data SET value=0 WHERE path = 'dev/js/merge_files';
    UPDATE core_config_data SET value=0 WHERE path = 'dev/css/merge_css_files';"
echo ${query}

# echo "---------------- Execute query ----------------"
mysql -u${SITE} -p${SITE} ${SITE} <<< ${query}
# echo "---------------- Query executed ----------------"
