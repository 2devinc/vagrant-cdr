#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/../../../config.sh"

### INSTALL N98-MAGERUN
if [ ! -f "$FILE" ]
then
    curl -sS -O https://files.magerun.net/n98-magerun.phar
    chmod +x ./n98-magerun.phar && mv n98-magerun.phar $FILE
fi