#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/../../../config.sh"

cd $WWW_FOLDER
n98-magerun admin:user:create $USERNAME $EMAIL_ADMIN $PASSWORD_ADMIN $FIRSTNAME $LASTNAME $ROLE