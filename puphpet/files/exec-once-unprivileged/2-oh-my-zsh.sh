#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/../../../config.sh"

### INSTALL OH-MY-ZSH
if [ -d "$ohmyzsh" ]
then
    rm -rf "$ohmyzsh"
fi

if [ ! -d "$ohmyzsh" ]
then
    ### Added zsh shell.
    wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh 
    sudo chsh -s /bin/zsh vagrant
    zsh
fi