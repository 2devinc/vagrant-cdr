#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/../../../config.sh"

composer_install() {
    echo "Installing composer packages..."
    composer install --no-progress --no-suggest --optimize-autoloader --no-interaction
    echo "Finished installing."
}

execute_script() {
    cd "$WWW_FOLDER"
    composer_install
}

execute_script
