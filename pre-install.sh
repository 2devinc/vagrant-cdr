#!/usr/bin/env bash

source config.sh

########## Create sources folder from repo ##########
mySources() {
    ########## Get sources in Git ##########
    echo "--------- cloning GIT repo ---------"
    if [ ! -d $SOURCE_FOLDER ]; then
        echo "Create sources directory"
        mkdir "$SOURCE_FOLDER"
    elif [ -d "$SOURCE_FOLDER" ]; then
        echo "Remove content sources directory"
        rm -rf "$SOURCE_FOLDER"
    fi
    git clone $REPO $SOURCE_FOLDER
    echo "--------- cloning GIT repo DONE ---------"
}

########## Download and move DB ##########
myDatabase() {
    echo "--------- retrieving DB backup from stage ---------"
    ssh-keyscan -H 2dev.client.ecritel.ca >> ~/.ssh/known_hosts
    if [ ! -d "$DB_FOLDER" ]; then
        echo "Create database directory"
        mkdir $DB_FOLDER
    elif [ -d "$DB_FOLDER" ]; then
        echo "Remove database directory content"
        rm -rf $DB_FOLDER/*
    fi
    rsync -rva --progress 2dev@2dev.client.ecritel.ca:/home/DB-backup/$SITE-stage.sql.gz $DB_FOLDER
    gzip -d $DB_FOLDER/$SITE-stage.sql.gz
    vagrant ssh -c "cd /opt/puphpet-state && sudo rm -rf exec-once* && sudo rm -rf db-import*"
    vagrant reload --provision
    echo "--------- retrieving DB backup from stage DONE ---------"
}

########## Download and move media ##########
myMedia() {
    echo "--------- retrieving media WYSIWYG from stage ---------"
    ssh-keyscan -H 2dev.client.ecritel.ca >> ~/.ssh/known_hosts
    rsync -rva --progress 2dev@2dev.client.ecritel.ca:/home/DB-backup/$SITE-media-wysiwyg.tar.gz .
    tar -xvzf $SITE-media-wysiwyg.tar.gz
    rm -rf $SOURCE_FOLDER/media/wysiwyg
    rm -rf $SITE-media-wysiwyg.tar.gz
    if [ ! -d "$SOURCE_FOLDER/media" ]; then
        mkdir $SOURCE_FOLDER/media
    fi
    mv wysiwyg $SOURCE_FOLDER/media/
    echo "--------- retrieving media WYSIWYG from stage DONE ---------"
}

generateLocalConfig() {
    echo "--------- Generate local.xml ---------"
    cd $SOURCE_FOLDER
    wget --no-verbose https://files.magerun.net/n98-magerun.phar
    chmod +x ./n98-magerun.phar
    ./n98-magerun.phar local-config:generate localhost $SITE $SITE $SITE files admin
    echo "--------- Generate local.xml DONE ---------"
}

vagrantUp() {
    ########## Launch Vagrant Up ##########
    echo "--------- Create Box Vagrant ---------"
    cd -
    $VAGRANT up
}

case "$1" in
  sources)
    mySources
    ;;
  database)
    myDatabase
    ;;
  media)
    myMedia
    ;;  
  install)
    mySources
    myDatabase
    myMedia
    generateLocalConfig
    vagrantUp
    ;;
  *)
    echo "Command not found"
    ;;
esac
